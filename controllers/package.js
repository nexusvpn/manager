const responseManager = require('../manager/responseManager');
const packagesModel = require("../models/database.js").dataModels.packagesModel;
const helper = require('./helper');

exports.list = function(req, res){
    const _id = req.query._id;
    const platform = req.query.platform;

    let query = {};

    if(_id && platform){
        query._id = _id;
        query.platform = platform;
    }
    packagesModel.find(query, {__v: 0}, function(e, data){
        if(!e && data.length > 0){
            return res.send({
                resCode: responseManager.success_code,
                data: data
            });
        }else{
            return res.send({
                resCode: responseManager.failure_code,
                message: 'No packages found'
            });
        }
    });
};

exports.update = function(req, res){
    const body = req.body;
    packagesModel.findOneAndUpdate({_id: body._id, platform: body.platform}, body, function(e, data){
        return res.send({
            resCode: responseManager.success_code,
            message: 'Package updated'
        });
    });
};

exports.delete = function(req, res){
    const body = req.body;
    packagesModel.findOneAndRemove({_id: body._id, platform: body.platform}, function(e, data){
        return res.send({
            resCode: responseManager.success_code,
            message: 'Package deleted'
        });
    });
};

exports.add = function(req, res){
    const body = req.body;
    packagesModel.find({name: body.name, platform: body.platform}, function(e, data){
        if(!e && data.length > 0){
            return res.send({
                resCode: responseManager.failure_code,
                message: 'Package name already exist for given platform'
            });
        }else{
            new packagesModel(body).save(function (e, newPackage) {});
            return res.send({
                resCode: responseManager.success_code,
                message: 'New package saved'
            });
        }
    });
};