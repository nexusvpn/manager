const responseManager = require('../manager/responseManager');
const proxiesModel = require("../models/database.js").dataModels.proxiesModel;
const helper = require('./helper');


exports.list = function(req, res){

    //Set Header for accepting requests
    helper.setHeader(res, '*');

    const host = req.query.host;
    let query = {};

    if(host){
        query = {host: host};
    }

    proxiesModel.find(query, {_id: 0, __v: 0}, function(e, data){
        if(!e && data.length > 0){
            return res.send({
                resCode: responseManager.success_code,
                data: data
            });
        }else{
            return res.send({
                resCode: responseManager.failure_code,
                message: 'No proxies found'
            });
        }
    });
};

exports.update = function(req, res){
    const body = req.body;
    proxiesModel.findOneAndUpdate({host: body.host}, req.body, function(e, data){
        return res.send({
            resCode: responseManager.success_code,
            message: 'Proxy updated'
        });
    });
};

exports.delete = function(req, res){
    const body = req.body;
    proxiesModel.findOneAndRemove({host: body.host}, function(e, data){
        return res.send({
            resCode: responseManager.success_code,
            message: 'Proxy deleted'
        });
    });
};

exports.add = function(req, res){
    const body = req.body;
    proxiesModel.find({host: body.host}, function(e, data){
        if(!e && data.length > 0){
            return res.send({
                resCode: responseManager.failure_code,
                message: 'Proxy already exists or try changing parameter values'
            });
        }else{
            new proxiesModel(body).save(function (e, newProxy) {});
            return res.send({
                resCode: responseManager.success_code,
                message: 'New proxy saved'
            });
        }
    });
};
