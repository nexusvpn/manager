var fs = require("fs");
var config = JSON.parse(fs.readFileSync(__dirname + "/../config.json"));
var db_user = config.db_user;
var db_pass = config.db_pass;
var host = config.host;
var port = config.port;
var dbs = config.database;
var uri;

var mongoose = require("mongoose");
uri = 'mongodb://'+db_user+':'+db_pass+"@"+host+":"+port+"/"+dbs;
console.log(uri);
mongoose.connect(uri, function(){

}, {useMongoClient: true});
mongoose.set('debug', true);
module.exports.mongoose = mongoose;

var dbSchema = require("./schema.js");
//Create Shared Models here
module.exports.dataModels = {
    proxiesModel: mongoose.model('proxies', dbSchema.proxies()),
    packagesModel: mongoose.model('packages', dbSchema.packages())
};
