
function logger(msg){
    var debug = config.logger_debug;
    if(debug){
        if(typeof msg == 'object') {
            console.log('-DEBUG-');
            console.log(msg);
        }else{
            console.log('-DEBUG- '+ msg);
        }
    }
}

/** Search table **/
function searchTable(id, keyword) {
    console.log(keyword);
    // Declare variables
    var filter, table, tr, td, i;
    filter = keyword.toUpperCase();
    table = document.getElementById(id);
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query

    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

/** Show loader **/
function showLoader(id) {
    $('#' + id).css('display', 'block');
}

/** Hide loader **/
function hideLoader(id) {
    $('#' + id).css('display', 'none');
}

/** Retrieves values from query string **/

function getQueryString(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    return false;
}

/** Custom ajax call function
 * service: api end point
 * params: parameters for passing as a object **/

function ajaxCall(service, params, callback) {
    var url = config.server_url + service.url;
    var type = service.type;

    showLoader('loader');

    logger(url, params, type);

    $.ajax({
        url: url,
        type: type,
        data: params,
        dataType: 'json',
        success: function (success) {
            hideLoader('loader');
            callback(success);
        },
        error: function (error) {
            hideLoader('loader');
            callback(error);
        }
    });
}

/** Remove item from array **/
Array.prototype.remove = function (from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};

Array.prototype.compact = function () {
    return this.filter(function (x) {
        return x !== undefined;
    });
};

Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
};

Date.prototype.addHours = function (hours) {
    this.setTime(this.getTime() + (hours * 60 * 60 * 1000));
    return this;
};

Date.prototype.addMinutes = function (minutes) {
    var date = new Date(this.getTime());
    return new Date(date.getTime() + minutes * 60000);
};

Array.prototype.clean = function (deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};
