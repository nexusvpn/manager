
var config = {
  server_url: '',
  scopes: 'manage_pages',
  logger_debug: true,
  fb_app_id: '154251628520407',
  users_prefix_api: '/api/users'
};

logger(window.user);

var services = {
  users: {
      get: {
          url: config.users_prefix_api + '/get',
          type: 'GET'
      }
  }
};
