

getProxies();

function getProxies(){
    $.ajax({
        type: 'get',
        url: '/api/proxies',
        dataType: 'json',
        success: function (response) {
            if(response.resCode == 200){
                var con = '';
                con += '<ul class="list-group">';
                for(var i = 0; i < response.data.length; i++) {
                    con += '<a title="Update proxy" href="#" onclick="updateProxy(\'' + response.data[i].host + '\');" class="list-group-item list-group-item-action list-group-item-warning">';
                    con += '#: ' + i + '<br>';
                    con += 'External Name: ' + response.data[i].name + '<br>';
                    con += 'Internal Name: ' + (response.data[i].internal_name ?  response.data[i].internal_name : "Not found") + '<br>';
                    con += 'Data Center: ' + (response.data[i].data_center ?  response.data[i].data_center : "Not found") + '<br>';
                    con += 'Host: ' + response.data[i].host + '<br>';
                    con += 'Method: ' + response.data[i].method + '<br>';
                    con += 'Port: ' + response.data[i].port + '<br>';

                    var password = response.data[i].password;
                    var hiddenPassword = "";
                    for(var k = 0; k < password.length; k++){
                        hiddenPassword += "*";
                    }
                    con += 'Password: ' + hiddenPassword;
                    con += '<input type="hidden" value="'+response.data[i].host+'" id="'+i+'">';
                    con += '</a>';
                    con += '<br>';

                }
                con += '</ul>';
                $('#data').html(con);
            }else{
                responseStatus('info', 'No proxies found - <a href="/proxies/add">Add new</a>');
            }
        },
        error: function (err) {
            responseStatus('danger', 'Something went wrong getting proxies data');
        }
    })
}

function updateProxy(host) {
    window.location = '/proxies/update?host='+host;
    return false;
}


function responseStatus(status, msg) {
    var con = '';
    con += '<div id="success" class="alert alert-'+status+'">';
    con += '<strong>'+status+': </strong> '+msg+'';
    con += '</div>';
    $('#response_text').html(con);
}