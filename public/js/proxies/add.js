
$('form').on('submit', function () {

    var name = $('#name').val();
    var host = $('#host').val();
    var port = $('#port').val();
    var method = $('#method').val();
    var password = $('#password').val();
    var internal_name = $('#internal_name').val();
    var data_center = $('#data_center').val();


    var data = {
        name: name,
        host: host,
        port: port,
        method: method,
        password: password,
        internal_name: internal_name,
        data_center: data_center
    };

    $.ajax({
        type: 'post',
        url: '/api/proxies',
        dataType: 'json',
        data: data,
        success: function (response) {
            if (response.resCode == 200) {
                responseStatus('success', response.message);
            }else{
                responseStatus('warning', response.message);
            }
        },
        error: function (err) {
                responseStatus('danger', err);
        }
    });
   return false;
});

function responseStatus(status, msg) {
    var con = '';
    con += '<div id="success" class="alert alert-'+status+'">';
    con += '<strong>'+status+'!</strong> '+msg+'';
    con += '</div>';
    $('#response_text').html(con);

    setTimeout(function () {
        $('#response_text').html('');
    }, 5000)
}
