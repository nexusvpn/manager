



function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var host = getParameterByName('host');

getProxy();

function getProxy(){
    $.ajax({
        type: 'get',
        url: '/api/proxies',
        data: {host: host},
        dataType: 'json',
        success: function (response) {
            if(response.resCode == 200){
                var con = '';
                var data = response.data[0];
                var keys = Object.keys(data);

                for(var i = 0; i < keys.length; i++){
                    var key = keys[i];
                    var value = data[key];
                    $('#'+key).val(value);
                }
            }else{
                responseStatus('info', 'No proxies found - <a href="/proxies/add">Add new</a>');
            }
        },
        error: function (err) {
            responseStatus('danger', 'Something went wrong getting proxies data');
        }
    })
}

function updateForm() {

    var a = confirm('Are you sure you want to update this proxy?');

    if(a) {
        var name = $('#name').val();
        var host = $('#host').val();
        var port = $('#port').val();
        var method = $('#method').val();
        var password = $('#password').val();
        var internal_name = $('#internal_name').val();
        var data_center = $('#data_center').val();


        var data = {
            name: name,
            host: host,
            port: port,
            method: method,
            password: password,
            internal_name: internal_name,
            data_center: data_center
        };


        $.ajax({
            type: 'put',
            url: '/api/proxies',
            dataType: 'json',
            data: data,
            success: function (response) {
                if (response.resCode == 200) {
                    responseStatus('success', response.message);
                } else {
                    responseStatus('warning', response.message);
                }
            },
            error: function (err) {
                responseStatus('danger', err);
            }
        });
    }
    return false;
}

function deleteProxy() {
    var a = confirm('Are you sure you want to delete this proxy?');
    if(a) {
        $.ajax({
            type: 'delete',
            url: '/api/proxies',
            data: {host: host},
            dataType: 'json',
            success: function (response) {
                if (response.resCode == 200) {
                    window.location.reload();
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    }
}

function responseStatus(status, msg) {
    var con = '';
    con += '<div id="success" class="alert alert-'+status+'">';
    con += '<strong>'+status+'!</strong> '+msg+'';
    con += '</div>';
    $('#response_text').html(con);

    setTimeout(function () {
        $('#response_text').html('');
    }, 5000)
}

function cancelUpdate() {
    window.location = '/proxies/list';
    return false;
}
