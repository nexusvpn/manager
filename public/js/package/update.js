
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var _id = getParameterByName('_id');
var platform = getParameterByName('platform');

getPackage();

function getPackage(){
    $.ajax({
        type: 'get',
        url: '/api/package',
        data: {_id: _id, platform: platform},
        dataType: 'json',
        success: function (response) {
            if(response.resCode == 200){

                var data = response.data[0];
                var keys = Object.keys(data);

                for(var i = 0; i < keys.length; i++){
                    var key = keys[i];
                    var value = data[key];
                    $('#'+key).val(value);
                }
            }else{
                responseStatus('info', 'No package found - <a href="/package/add">Add new</a>');
            }
        },
        error: function (err) {
            responseStatus('danger', 'Something went wrong getting package data');
        }
    })
}

function updateForm() {

    var a = confirm('Are you sure you want to update this package?');

    if(a) {
        var name = $('#name').val();
        var platform = $('#platform').val();


        var data = {
            name: name,
            platform: platform,
            _id: _id
        };


        $.ajax({
            type: 'put',
            url: '/api/package',
            dataType: 'json',
            data: data,
            success: function (response) {
                if (response.resCode == 200) {
                    responseStatus('success', response.message);
                } else {
                    responseStatus('warning', response.message);
                }
            },
            error: function (err) {
                responseStatus('danger', err);
            }
        });
    }
    return false;
}

function deletePackage() {
    var a = confirm('Are you sure you want to delete this package?');
    if(a) {
        $.ajax({
            type: 'delete',
            url: '/api/package',
            data: {_id: _id, platform: platform},
            dataType: 'json',
            success: function (response) {
                if (response.resCode == 200) {
                    window.location.reload();
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    }
}

function responseStatus(status, msg) {
    var con = '';
    con += '<div id="success" class="alert alert-'+status+'">';
    con += '<strong>'+status+'!</strong> '+msg+'';
    con += '</div>';
    $('#response_text').html(con);

    setTimeout(function () {
        $('#response_text').html('');
    }, 5000)
}

function cancelUpdate() {
    window.location = '/package/list';
    return false;
}
