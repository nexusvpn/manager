
$('form').on('submit', function () {

    var name = $('#name').val();
    var platform = $('#platform').val();

    var data = {
        name: name,
        platform: platform
    };

    $.ajax({
        type: 'post',
        url: '/api/package',
        dataType: 'json',
        data: data,
        success: function (response) {
            if (response.resCode == 200) {
                responseStatus('success', response.message);
            }else{
                responseStatus('warning', response.message);
            }
        },
        error: function (err) {
            responseStatus('danger', err);
        }
    });
    return false;
});

function responseStatus(status, msg) {
    var con = '';
    con += '<div id="success" class="alert alert-'+status+'">';
    con += '<strong>'+status+'!</strong> '+msg+'';
    con += '</div>';
    $('#response_text').html(con);

    setTimeout(function () {
        $('#response_text').html('');
    }, 5000)
}
