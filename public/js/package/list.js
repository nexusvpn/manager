

getPackages();

function getPackages(){
    $.ajax({
        type: 'get',
        url: '/api/package',
        dataType: 'json',
        success: function (response) {
            if(response.resCode == 200){
                var con = '';
                con += '<ul class="list-group">';
                for(var i = 0; i < response.data.length; i++) {
                    con += '<a title="Update package" href="#" onclick="updatePackage(\'' + response.data[i]._id + '\', \'' + response.data[i].platform + '\');" class="list-group-item list-group-item-action list-group-item-warning">';
                    con += 'Name: ' + response.data[i].name + '<br>';
                    con += 'Platform: ' + response.data[i].platform + '<br>';
                    con += '</a>';
                    con += '<br>';

                }
                con += '</ul>';
                $('#data').html(con);
            }else{
                responseStatus('info', 'No packages found - <a href="/package/add">Add new</a>');
            }
        },
        error: function (err) {
            responseStatus('danger', 'Something went wrong getting packages data');
        }
    })
}

function updatePackage(_id,platform) {
    window.location = '/package/update?_id='+_id+'&platform='+platform;
    return false;
}


function responseStatus(status, msg) {
    var con = '';
    con += '<div id="success" class="alert alert-'+status+'">';
    con += '<strong>'+status+': </strong> '+msg+'';
    con += '</div>';
    $('#response_text').html(con);
}