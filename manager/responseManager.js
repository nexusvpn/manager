
var responseManager = {
    success_code: 200,
    failure_code: 400,
    success_message: 'SUCCESS',
    failure_message: 'FAILURE'
};

module.exports = responseManager;
