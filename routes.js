
var proxiesCtlr = require('./controllers/proxies');
var packageCtlr = require('./controllers/package');
var auth = require('http-auth');

var basic = auth.basic({
    realm: "Restricted Area!",
    file: __dirname + "/.htpasswd"
});

module.exports = function(app){
    var config = app._config;

    /** VIEW ROUTES **/
    app.get('/', auth.connect(basic), function(req, res, next) {
      res.render('index', { title: config.app_name });
    });

    app.get('/proxies/list', auth.connect(basic), function(req, res, next) {
        res.render('proxies/list', { title: config.app_name });
    });

    app.get('/proxies/add', auth.connect(basic), function(req, res, next) {
        res.render('proxies/add', { title: config.app_name });
    });


    app.get('/proxies/update', auth.connect(basic), function(req, res, next) {
        res.render('proxies/update', { title: config.app_name });
    });

    app.get('/navigation', auth.connect(basic), function(req, res, next) {
        res.render('navigation', { title: config.app_name });
    });

    app.get('/package/list', auth.connect(basic), function(req, res, next) {
        res.render('package/list', { title: config.app_name });
    });

    app.get('/package/add', auth.connect(basic), function(req, res, next) {
        res.render('package/add', { title: config.app_name });
    });

    app.get('/package/update', auth.connect(basic), function(req, res, next) {
        res.render('package/update', { title: config.app_name });
    });


    /** API ROUTES **/
    app.get('/api/proxies', proxiesCtlr.list);
    app.post('/api/proxies', proxiesCtlr.add);
    app.put('/api/proxies', proxiesCtlr.update);
    app.del('/api/proxies', proxiesCtlr.delete);

    app.get('/api/package', packageCtlr.list);
    app.post('/api/package', packageCtlr.add);
    app.put('/api/package', packageCtlr.update);
    app.del('/api/package', packageCtlr.delete);

};
