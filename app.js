var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var https = require('https');
var fs = require('fs');
var config = JSON.parse(fs.readFileSync("./config.json"));
var server_port_http = config.server_port_http;
var server_port_https = config.server_port_https;
var logger_debug = config.logger_debug;


var app = express();
app._config = config;
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


require('./routes')(app);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

/*var options = {
    ca: fs.readFileSync('/root/ssl_certs/chain.pem'),
    key: fs.readFileSync('/root/ssl_certs/privkey.pem'),
    cert: fs.readFileSync('/root/ssl_certs/cert.pem')
};
var server = https.createServer(options, app);
var io = require('socket.io')(server);
global.io = io;
io.on('connection', function(client) {
    console.log('Client connected...');
});*/




var listener = app.listen(server_port_http, function(){
    console.log('Server is listening on port ' + listener.address().port);
});

module.exports = app;
